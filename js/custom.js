
$(document).ready(function () {

    //About Us Top Carousel
    $('.banner-carousel').slick({
        autoplay: true,
        arrows: false,
        dots: true,
        swipe: true,
        touchMove: true
    });

    //Achievement Carousel
    $('.achievement-carousel').slick({
        autoplay: true,
        arrows: true,
        dots: true,
        swipe: true,
        touchMove: true
    });

    //Secondary Nav Item Active
    $('.second-nav-list-item').click(function () {
       $('.second-nav-list-item').removeClass('second-nav-active');
       $(this).addClass('second-nav-active');
    });

    //Footer Mobile Collapse Plus Minus
    var footerMobileCollapseIds = $('#our_service_collapse, #support_collapse, #our_company_collapse, #legal_collapse, #useful_links_collapse');
    $(function ($) {
        footerMobileCollapseIds.on('show.bs.collapse hidden.bs.collapse', function () {
            $(this).prev().find('.fa').toggleClass('fa-plus fa-minus');
        })
    });

});

